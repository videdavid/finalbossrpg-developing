// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomRPGCharacter.h"

// Sets default values
ACustomRPGCharacter::ACustomRPGCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACustomRPGCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACustomRPGCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACustomRPGCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

//Allow translation in Z axis during root motion
FVector ACustomRPGCharacter::ConstrainAnimRootMotionVelocity(const FVector& RootMotionVelocity, const FVector& CurrentVelocity) const
{
	return RootMotionVelocity;
}

