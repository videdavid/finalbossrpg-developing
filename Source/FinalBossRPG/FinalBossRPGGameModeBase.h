// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FinalBossRPGGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FINALBOSSRPG_API AFinalBossRPGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
